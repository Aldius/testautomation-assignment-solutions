package com.epam.training.test_backend.model;

import java.util.List;

public class Event {

	private int id;
	private String title;
	private String type;
	private List<Integer> start;
	private List<Integer> end;

	public List<Integer> getStart() {
		return start;
	}

	public void setStart(List<Integer> start) {
		this.start = start;
	}

	public List<Integer> getEnd() {
		return end;
	}

	public void setEnd(List<Integer> end) {
		this.end = end;
	}

	public int getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public String getType() {
		return type;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setType(String type) {
		this.type = type;
	}

}
