package com.epam.assignment_elte.utils;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MathTest {

	private Math underTest;

	@Before
	public void setUp() {
		underTest = new Math();
	}

	@Test
	public void testGreatestCommonDivisorSimple() {
		final int a = 60;
		final int b = 24;

		final int result = underTest.greatesCommonDivisor(a, b);

		Assert.assertEquals(12, result);
	}

	@Test
	public void testGreatestCommonDivisorTricky() {
		final int a = 2;
		final int b = 1;

		final int result = underTest.greatesCommonDivisor(a, b);

		Assert.assertEquals(1, result);
	}

	@Test
	public void testGreatestCommonDivisorZeroA() {
		final int a = 0;
		final int b = 10;

		final int result = underTest.greatesCommonDivisor(a, b);

		Assert.assertEquals(10, result);
	}

	@Test
	public void testGreatestCommonDivisorZeroB() {
		final int a = 20;
		final int b = 0;

		final int result = underTest.greatesCommonDivisor(a, b);

		Assert.assertEquals(20, result);
	}

	@Test
	public void testInterestRate() {
		final double base = 10.0;
		final double interestRate = 0.1;
		final int years = 5;

		final long result = underTest.interestRate(base,interestRate,years);

		Assert.assertEquals(16, result);

	}

}