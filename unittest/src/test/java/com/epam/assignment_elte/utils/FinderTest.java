package com.epam.assignment_elte.utils;

import org.assertj.core.util.Lists;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class FinderTest {

	private Finder underTest;
	private List<Integer> list;

	@Before
	public void setUp() {
		underTest = new Finder();
		list = Lists.newArrayList(10, 20, 30, 40, 50, 60);
	}

	@Test
	public void testSimple() {
		final Integer target = 50;

		final int location = underTest.find(list, target);

		Assert.assertEquals(5, location);
	}

	@Test
	public void testNoLoc() {
		final Integer target = 70;

		final int location = underTest.find(list, target);

		Assert.assertEquals(-1, location);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testListNull() {
		final List<Integer> list = null;
		final Integer target = 50;

		underTest.find(list, target);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testTargetNull() {
		final Integer target = null;

		underTest.find(list, target);
	}

}