package com.epam.assignment_elte.utils;

import org.assertj.core.util.Lists;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class StringUtilsTest {

	private StringUtils underTest;

	@Before
	public void setUp() {
		underTest = new StringUtils();
	}

	@Test
	public void testJoinLessThanFour() {
		final String start = "start";
		final String end = "end";
		final List<String> list = Lists.newArrayList("one", "two", "three");

		final String result = underTest.join(start, end, list);

		Assert.assertEquals("startonetwothreeend", result);
	}

	@Test
	public void testJoinMoreThanFour() {
		final String start = "start";
		final String end = "end";
		final List<String> list = Lists.newArrayList("one", "two", "three", "four", "five");

		final String result = underTest.join(start, end, list);

		Assert.assertEquals("startfivefourthreetwooneend", result);
	}

	@Test
	public void testJoinEmpty() {
		final String start = "start";
		final String end = "end";
		final List<String> list = Lists.emptyList();

		final String result = underTest.join(start, end, list);

		Assert.assertEquals("startemptyend", result);
	}

	@Test
	public void testnthElementsOnlySimple() {
		final String input = "1234567";
		final int n = 3;

		final String result = underTest.nthElementsOnly(input, n);

		Assert.assertEquals("36", result);
	}

	@Test
	public void testnthElementsOnlyEmpty() {
		final String input = "";
		final int n = 3;

		final String result = underTest.nthElementsOnly(input, n);

		Assert.assertEquals("", result);
	}

}