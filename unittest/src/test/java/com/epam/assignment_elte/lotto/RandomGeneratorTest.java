package com.epam.assignment_elte.lotto;

import net.bytebuddy.pool.TypePool;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Random;

@RunWith(MockitoJUnitRunner.class)
public class RandomGeneratorTest {

	@Mock
	private Random random;

    private RandomGenerator underTest;

    @Before
    public void setUp() {
    	underTest = new RandomGenerator(random);
    }

    @Test
	public void testGetNumberSimple() {
		final int from = 1;
		final int to = 3;

		Mockito.when(random.nextInt(to - from + 1)).thenReturn(2);
		final int result = underTest.getNumber(from, to);

		Assert.assertEquals(3, result);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetNumberException() {
		final int result = underTest.getNumber(6, 2);
	}

}