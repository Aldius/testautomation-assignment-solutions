package com.epam.assignment_elte.lotto;

import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;

public class UniquenessVerifierTest {

	private UniquenessVerifier underTest;

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Before
	public void setUp() {
		underTest = new UniquenessVerifier();
	}

	@Test
	public void testUniqueList() {
		final List<Integer> list = Lists.newArrayList(1, 2, 3, 4, 5, 6);
		underTest.verify(list);
	}

	@Test
	public void testUniqueException() {
		thrown.expect(RuntimeException.class);
		thrown.expectMessage("non unique number detected");

		final List<Integer> list = Lists.newArrayList(2, 2, 3, 4, 5, 6);
		underTest.verify(list);
	}

}