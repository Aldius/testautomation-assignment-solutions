package com.epam.assignment_elte.lotto;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.assertj.core.api.Java6Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class LottoNumbersTest {

	private LottoNumbers underTest;

	@Before
	public void setUp() {
		underTest = new LottoNumbers(new RandomGenerator(new Random()), new UniquenessVerifier());
	}

	@Test
	public void testGenerateNumbers() {
		final int from = 1;
		final int to = 90;
		final int count = 5;

		final List<Integer> result = underTest.generateNumbers(from, to, count);

		assertThat(result)
				.isSubsetOf(IntStream.rangeClosed(from, to).boxed().collect(Collectors.toList()))
				.allSatisfy(number -> assertThat(result).containsOnlyOnce(number))
				.hasSize(count);
	}

}