package hu.elte.trello.pagemodels;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Instant;

public class CardPage extends BasePage {

	@FindBy(className = "js-add-a-card")
	public WebElement toDoList;

	@FindBy(className = "list-card-composer-textarea")
	public WebElement textBox;

	public CardPage(final WebDriver driver) {
		super(driver);
	}

	public static CardPage create(final WebDriver driver) {
		return PageFactory.initElements(driver, CardPage.class);
	}

	public void addCard() {
		final WebElement toDoList = getList("Todo");
		final WebElement addACardButton = toDoList.findElement(By.className("open-card-composer"));
		addACardButton.click();
		final WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.className("list-card-composer-textarea")));
		final WebElement textArea = toDoList.findElement(By.className("list-card-composer-textarea"));
		textArea.sendKeys("o HÁ " + Instant.now());
		textArea.sendKeys(Keys.RETURN);
	}

	public String moveTheCard() {
		final WebElement toDoList = getList("Todo");
		final WebElement firstCard = toDoList.findElement(By.className("ui-droppable"));
		final String cardName = firstCard.findElement(By.xpath("//*[contains(text(), 'o HÁ')]")).getAttribute("innerHTML");
		final Actions actions = new Actions(driver);
		final Action dragAndDrop = actions.dragAndDrop(firstCard, getList("Done")).build();
		dragAndDrop.perform();
		return cardName;
	}

	public boolean isCardMoved(final String movedCardName) {
		return getList("Done").getAttribute("innerHTML").contains(movedCardName);
	}

	private WebElement getList(final String listName) {
		return driver.findElements(By.className("list")).stream()
				.filter(webElement -> webElement.findElements(By.cssSelector("h2")).stream().anyMatch(webElement2 -> webElement2.getAttribute("innerHTML").equals(listName)))
				.findFirst()
				.orElseThrow(() -> new IllegalStateException(listName + "list not found!"));
	}

}
