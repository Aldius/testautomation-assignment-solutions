package hu.elte.trello.pagemodels;

import hu.elte.trello.entitites.LoginModel;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class LoginPage extends BasePage {

	@FindBy(how = How.ID, using = "user")
	private WebElement usernameField;

	@FindBy(how = How.ID, using = "password")
	private WebElement passwordField;

	@FindBy(how = How.ID, using = "login")
	private WebElement loginButton;

	public LoginPage(final WebDriver driver) {
		super(driver);
		layerLocator = By.className("all-boards");
	}

	public static LoginPage create(final WebDriver driver) {
		return PageFactory.initElements(driver, LoginPage.class);
	}

	public void navigateTo(final String url) {
		driver.get(url);
	}

	public void fillLoginForm(LoginModel model) {
		usernameField.sendKeys(model.getUsername());
		passwordField.sendKeys(model.getPassword());
		loginButton.click();
	}

}
