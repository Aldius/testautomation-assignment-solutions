package hu.elte.trello.pagemodels;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BoardsPage extends BasePage {

	@FindBy(css = "div[title='Adam_Horvath']")
	public WebElement table;

	public BoardsPage(final WebDriver driver) {
		super(driver);
		layerLocator = By.id("board");
	}

	public static BoardsPage create(final WebDriver driver) {
		return PageFactory.initElements(driver, BoardsPage.class);
	}

	public void selectTable() {
		table.click();
	}

}
