package hu.elte.trello.pagemodels;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.util.concurrent.TimeUnit;

public class BasePage {

	protected WebDriver driver;
	protected By layerLocator = By.tagName("body");

	public BasePage(WebDriver driver) {
		this.driver = driver;
	}

	public boolean isPageDisplay() {
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
				.withTimeout(30, TimeUnit.SECONDS)
				.pollingEvery(1, TimeUnit.SECONDS)
				.ignoring(NoSuchElementException.class);

		WebElement foo = wait.until(driver -> driver.findElement(layerLocator));

		return foo != null;
	}

}
