package hu.elte.trello.tests;

import hu.elte.trello.entitites.LoginModel;
import hu.elte.trello.pagemodels.BoardsPage;
import hu.elte.trello.pagemodels.CardPage;
import hu.elte.trello.pagemodels.LoginPage;
import org.junit.Assert;
import org.junit.Test;

public class Homework extends TestBase {

	public static final String TRELLO_LOGIN = "https://trello.com/login";
	public static final String USERNAME = "elte.webdriver@gmail.com";
	public static final String PASSWORD = "WebDriver";

	@Test
	public void trelloTest() {
		final LoginPage loginPage = LoginPage.create(driver);
		loginPage.navigateTo(TRELLO_LOGIN);

		final LoginModel loginModel = LoginModel.builder().username(USERNAME).password(PASSWORD).build();
		loginPage.fillLoginForm(loginModel);
		Assert.assertTrue(loginPage.isPageDisplay());
		Assert.assertTrue(driver.getTitle().contains("Boards"));

		final BoardsPage boardsPage = BoardsPage.create(driver);
		boardsPage.selectTable();
		Assert.assertTrue(boardsPage.isPageDisplay());
		Assert.assertTrue(driver.getTitle().contains("Adam_Horvath"));

		final CardPage cardPage = CardPage.create(driver);
		cardPage.addCard();
		final String movedCardName = cardPage.moveTheCard();
		Assert.assertTrue(cardPage.isCardMoved(movedCardName));


	}

}
